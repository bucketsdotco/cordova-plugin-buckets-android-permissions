# Android Permissions

This plugin sets permissions not included by default in AndroidManifest.xml

## Installation

`$ cordova plugin add https://bitbucket.org/bucketsdotco/buckets-android-permissions.git`
